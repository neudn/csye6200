package neu.edu.csye6200.vehicle;

/**
 * CSYE 6200 Assignment #4 
 */

/**
 * CSYE 6200 Vehicle starter class
 * 
 * @author Nan Deng
 * ID: 001901828
 *
 */

import java.io.Serializable;
import java.util.ArrayList;
import java.util.logging.Logger;

public class VehicleRegistry implements Serializable{
	
	private static Logger log = Logger.getLogger("neu.edu.csye6200.vehile.VehicleRegistry");
	private static VehicleRegistry vehicleRegistry;
	private ArrayList<Vehicle> al;
	
	// constructor
	public VehicleRegistry(){
		al = new ArrayList<Vehicle>();
		log.info("a new VehicleRegistry is created");
	}
	
	// singleton parttern, create VehicleRegistry only once
	public static VehicleRegistry getInstance(){
		if(vehicleRegistry != null) { 
			return vehicleRegistry;
		} 
		return new VehicleRegistry();
	}
	
	public ArrayList<Vehicle> getVehicleRegistry(){
		return this.al;
	}
	
	// to add a vehicle
	public void addVehicle(Vehicle v){
		al.add(v);							// add to linkedlist
	}
	
	// to get a vehicle by the licensePlate
	public Vehicle getVehicle(String s){
		for(Vehicle u: al){
			if(u.licensePlate.equals(s)) return u;
		}
		return null;
	}
	
	// to remove a vehicle by the licensePlate
	public void removeVehicle(String s){
		for(Vehicle u: al){
			if(u.licensePlate.equals(s)) al.remove(u);
			log.info("a vehicle is deleted");
			return;
		}
		return;
	}
	
	// to print all the vehicles
	public void printAllVehicles(){
		System.out.println("Here are the vehicle information:");
		if(al.isEmpty()){ 
			System.out.println("We are out of stock");
		} else {
			for(Vehicle v: al){
				v.printVehicleInfo();
			}
		}
	}
	
	// to retrieve a vehicle by licensePlate via HashMap
	//	public Vehicle retrieveByLicense(String s){
	//		return h.get(s);
	//	}
	
	public void sortByPlate(){
		boolean hasChanged = false;
		
		for(int i = al.size() - 1; i >= 0; i--){
			hasChanged = false;
			for(int j = 0; j < i; j++){
				if(al.get(j).getLicensePlate().compareTo(al.get(j+1).getLicensePlate()) > 0){
					Vehicle temp = al.get(j);
					Vehicle first = al.get(j);
					Vehicle second = al.get(j+1);
					
					first = second;
					second = temp;
					
					al.set(j, first);
					al.set(j+1, second);
					
					hasChanged = true;
				}			
			}
			if(!hasChanged) return;
		}
	}
	
	public static VehicleRegistry run(VehicleRegistry vr){

		vr.addVehicle(new Vehicle("Sedan","Toyota","Corolla","gfedcba"));
		vr.addVehicle(new Vehicle("Sedan","Honda","Civic","dcbagfe"));
		vr.addVehicle(new Vehicle("Sedan","Hyundai","Elantra","abcdefg"));
		vr.addVehicle(new Vehicle("Sports","Chevy","Camaro","123dcba"));
		vr.addVehicle(new Vehicle("Sports","Ford","Mustang","789agfe"));
		vr.addVehicle(new Vehicle("SUV","Jeep","Compass","456defg"));
		vr.addVehicle(new Vehicle("SUV","Subaru","Forester","zyxRAI"));
		vr.addVehicle(new Vehicle("SUV","Volvo","XC60","IOP1fe"));
		vr.addVehicle(new Vehicle("Pick-up","Dodge","Ram","HIMdefg"));
		vr.addVehicle(new Vehicle("Pick-up","GMC","Sierra","010jkl"));
		vr.addVehicle(new Vehicle("Hatchback","VW","Golf","zyxOPQ"));
		vr.addVehicle(new Vehicle("Hatchback","Opel","Astra","QWE678"));
		vr.addVehicle(new Vehicle("Hatchback","Fiat","500","500500"));
		
		vr.sortByPlate();
		
		System.out.println("Make: LicensePlate");
		for(Vehicle v: vr.getVehicleRegistry()){
			System.out.println(v.make + ": " + v.licensePlate);
		}

		return vr;
	}
	
}
