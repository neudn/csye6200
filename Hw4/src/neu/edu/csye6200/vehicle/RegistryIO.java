package neu.edu.csye6200.vehicle;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Logger;

/**
 * CSYE 6200 Assignment #4 
 */

/**
 * CSYE 6200 RegistryIO class
 * 
 * @author Nan Deng
 * ID: 001901828
 *
 */

public class RegistryIO {
	
	private static Logger log = Logger.getLogger("neu.edu.csye6200.vehicle.RegistryIO");
	private static VehicleRegistry vehicleRegistry;
	
	public RegistryIO(){
		vehicleRegistry = VehicleRegistry.getInstance();
		log.info("a vehicleRegistry class is created");
	}
	
	// read the VehicleRegistry file through load
	public void load(String fileName){
		try{
			FileInputStream fin = new FileInputStream("fileName");
			ObjectInputStream ois = new ObjectInputStream(fin);
			vehicleRegistry = (VehicleRegistry) ois.readObject();
			ois.close();
			log.info("a vehicle registry list is retrieved from disk");
		} catch (Exception e){
			e.printStackTrace();
			log.severe("cannot retrieve the file info");
		}
	}
	
	// write the VehicleRegistry file
	public void save(VehicleRegistry vr, String fileName) {
		for(Vehicle v: vr.getVehicleRegistry()){
			save(v);
		}
		try{
			FileOutputStream fout = new FileOutputStream("fileName");
			ObjectOutputStream oos = new ObjectOutputStream(fout);
			
			oos.writeObject(vehicleRegistry);
			oos.close();
			log.info("a vehicle registry list is written to disk");
		} catch (Exception e){
			e.printStackTrace();
			log.severe("cannot retrieve the file info");
		}
	}
	
	// add a vehicle to existing vehicleRegistry file
	private void save(Vehicle v) {
		
		try{
			vehicleRegistry.addVehicle(v);
			log.info("a vehicle registry list is saved into arrayList");
		} catch (Exception e){
			e.printStackTrace();
			log.severe("cannot retrieve the file info");
		}
	}
	
	// last requirement, load the file I just saved
	public void run(VehicleRegistry vr){
		File file = new File("/users/dn/fileName.txt");
		if(!file.exists()){
			log.severe("file doesn't exist");
			try {
				file.createNewFile();
				log.info("created a new file");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				log.severe("cannot create a new file");
			}
		}
		save(vr, "fileName");
		load("fileName");
	}
	
	public static void main(String[] args){
		RegistryIO registryIO = new RegistryIO();
//		registryIO.vehicleRegistry.run();
		VehicleRegistry vr = VehicleRegistry.run(vehicleRegistry);
		new RegistryIO().run(vr);
	}
}
