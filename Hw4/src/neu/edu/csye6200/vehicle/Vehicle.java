package neu.edu.csye6200.vehicle;
/**
 * CSYE 6200 Assignment #3 
 */

/**
 * CSYE 6200 Vehicle starter class
 * 
 * @author Nan Deng
 * ID: 001901828
 *
 */
public class Vehicle {
	
	String type;
	String make;
	String model;
	String licensePlate;
	int passengers;
	int fuelCap;
	int modelYear;
// Note - in the lecture, we switched from using Miles Per Gallon (MPG) to using Kilometers Per Liter (KPL).
	double kpl; // <== so this should be 'kpl', not 'mpg'
	
	public Vehicle(){
		
	}
	
	public Vehicle(String type, String make, String model, String licensePlate){
		this.type = type;
		this.make = make;
		this.model = model;
		this.licensePlate = licensePlate;
	}
		
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getModelYear() {
		return modelYear;
	}

	public void setModelYear(int modelYear) {
		this.modelYear = modelYear;
	}

	public double getKpl() {
		return kpl;
	}

	public void setKpl(double kpl) {
		this.kpl = kpl;
	}

	public void setMake(String make) {
		this.make = make;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public void setLicensePlate(String licensePlate) {
		this.licensePlate = licensePlate;
	}

	public void setPassengers(int passengers) {
		this.passengers = passengers;
	}

	public void setFuelCap(int fuelCap) {
		this.fuelCap = fuelCap;
	}

	public String getMake(){
		return this.make;
	}
	
	public String getModel(){
		return this.model;
	}
	
	public String getLicensePlate(){
		return this.licensePlate;
	}
	
	public int getPassengers(){
		return this.passengers;
	}
	
	public int getFuelCap(){
		return this.fuelCap;
	}
	
	public double getKPL(){
		return this.kpl;
	}
	
	public int calcRange(){
		int range = (int) (this.fuelCap * this.kpl);
		return range;
	}
	
	public void printVehicleInfo(){
		System.out.println("This is the latest " + this.getModel() +".");
		System.out.println("It is built by " + this.getMake() + ".");
		System.out.println("Its license plate is " + this.getLicensePlate());
		System.out.println("This vehicle can seat " + this.getPassengers() + " passengers.");
		System.out.println("And can run " + this.calcRange() + " miles after each refuel.");
		System.out.println("");
	}
}
